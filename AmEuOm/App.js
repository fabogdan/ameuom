
import React from 'react';
// import { NavigationContainer } from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native';
import SignInPhone from './src/SignIn/Screens/SignInPhone';
import ForgotPassword from "./src/SignIn/Screens/ForgotPassword";
import SignInEmail from "./src/SignIn/Screens/SignInEmail";
import SignInNavigation from "./src/SignIn/nagivation/SignInNavigation";
import ChooseUserType from "./src/SignUp/ChooseUserType";
import CineEstiBeneficiar from "./src/SignUp/SignUpBeneficiar/CineEstiBeneficiar";
import CineEstiPrestator from "./src/SignUp/SignUpPrestator/CineEstiPrestator"
import UndePresteziServicii from "./src/SignUp/SignUpPrestator/UndePresteziServicii";
import CeStiiSaFaci from "./src/SignUp/SignUpPrestator/CeStiiSaFaci";

export default function App() {
  return (
        <SignInNavigation/>
  );
}
