import React from 'react';
import {SafeAreaView} from "react-navigation";
// import {Dimensions, Image, SafeAreaView, Text, TouchableOpacity, View} from 'react-native';
// import { Button, Layout, TopNavigation, Input, Icon } from '@ui-kitten/components';
// import auth from '@react-native-firebase/auth';
// import firestore from '@react-native-firebase/firestore';
// import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
//import SvgUri from 'react-native-svg-uri';
import {
    Dimensions,
    Image,
    View,
    TouchableOpacity,
    Text,
    TextInput
} from 'react-native';

import ChooseUserType from "../../SignUp/ChooseUserType";
import Logo from "../Assets/logo.svg";

export default function SignInPhone(props)
{
    const { navigation } = props;

    return (
        <SafeAreaView style={styles.container}>
            <Logo style={styles.logo}/>
            <View style={{paddingVertical: 50}}>
                <View style={[styles.textField, {marginTop: '15%', width: '80%'}]}>
                    {/*<Image source={ require (require('../assets/ChangePassword/lock.png'))} style={styles.textFieldIcon} />*/}
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                        // value={passwordValue}
                        placeholder='Numar de telefon'
                        // icon={renderIcon}
                        // secureTextEntry={secureTextEntry}
                        // onIconPress={onIconPress}
                        // onChangeText={setPasswordValue}
                    />
                </View>
                <View style={[styles.textField, {marginTop: '5%', width: '80%'}]}>
                    {/*<Image source={ require (require('../assets/ChangePassword/lock.png'))} style={styles.textFieldIcon} />*/}
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                        // value={passwordValue}
                        placeholder='Cod unic din mesaj'
                        // icon={renderIcon}
                        // secureTextEntry={secureTextEntry}
                        // onIconPress={onIconPress}
                        // onChangeText={setPasswordValue}
                    />
                </View>
            </View>

                <TouchableOpacity style={styles.signIn}
                    // onPress={()=> navigation.navigate('ClientSignUp')}
                >
                    <Text style={{fontSize: 20, fontFamily: 'Poppins-SemiBold', color: '#000000'}}>Sign in</Text>
                </TouchableOpacity>
                <Text style={{fontSize: 18, paddingVertical: 10, fontFamily: 'Poppins-Regular'}}>
                    sau
                </Text>
                <TouchableOpacity style={styles.signUp}
                    onPress={()=> navigation.navigate('SignInEmail')}
                >
                    <Text style={{fontSize: 20, fontFamily: 'Poppins-SemiBold', color: '#ffffff'}}>Sign in </Text>
                    <Text style={{fontSize: 20, color: '#ffffff', fontFamily: 'Poppins-Regular'}}>cu email</Text>
                </TouchableOpacity>

            <View style={{flexDirection: 'column', alignItems: 'center', paddingVertical: 30}}>

                <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')}>
                    <Text style={{color: '#ffffff', fontSize: 18, fontFamily: 'Poppins-SemiBold'}}>
                        Am uitat parola
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={ () => navigation.navigate('ChooseUserType')} style={{flexDirection: 'row', marginTop: 30}}>
                    <Text style={{color: '#000000', fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 5}}>
                        Nu ai deja un cont?
                    </Text>
                    <Text style={{color: '#ffffff', fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 5}}>
                        Sign Up
                    </Text>
                </TouchableOpacity>
            </View>

        </SafeAreaView>
    );
};

const styles = {

    container:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFC22A',
        paddingVertical: 180,
    },

    logo:{
        fill: '#000000',
    },

    textField: {
        width: 315,
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#000000',
        height: 40,
        borderRadius: 30,
        marginLeft: 30,
        marginRight: 30,
    },

    signIn: {
        width: '80%',
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        height: 40,
        borderRadius: 30,
        marginLeft: 30,
        marginRight: 30,
    },

    signUp: {
        width: '80%',
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000000',
        height: 40,
        borderRadius: 30,
        marginLeft: 30,
        marginRight: 30,
    },

};
