import React from 'react';
import {SafeAreaView} from "react-navigation";
import {
    Dimensions,
    Image,
    View,
    TouchableOpacity,
    Text,
    TextInput,
    StyleSheet
} from 'react-native';

import Beneficiar from "../SignIn/Assets/beneficiar.svg"
import Prestator from "../SignIn/Assets/prestator.svg"

export default function ForgotPassword(props)
{
    const { navigation } = props;

    return (

        <SafeAreaView style={styles.container}>

            <TouchableOpacity onPress={() => navigation.navigate('CineEstiPrestator') } style={[styles.card, {height: '50%', paddingTop: '10%'}]}>
                <Prestator style={[styles.logo, {fill: '#000000'}]}/>
            </TouchableOpacity>

            <View style={{width: '100%', paddingVertical: 15, alignItems: 'center', justifyContent: 'center', alignContent: 'center'}}>
                <Text style={{fontSize: 16, width: '90%', fontFamily: 'Poppins-Regular'}}>
                    Poti folosi aplicatia ca
                    <Text style={{ fontFamily: 'Poppins-SemiBold'}}> prestator</Text>,
                    <Text style={{ fontFamily: 'Poppins-SemiBold'}}> beneficiar </Text>
                    sau
                    <Text style={{ fontFamily: 'Poppins-SemiBold'}}> ambele</Text>
                    . Pentru inceput alege una
                    dintre variante.
                </Text>
            </View>

            <TouchableOpacity onPress={() => navigation.navigate('CineEstiBeneficiar')} style={[styles.card, {backgroundColor: '#000000', height:'60%'}]}>
                <View style={{backgroundColor: '#000000', width: '80%', justifyContent:'center', alignItems: 'center', alignContent:'center', marginTop: '10%'}}>
                    <Beneficiar style={styles.logo}/>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('SignInEmail')} style={{width: '100%', height: '20%',flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start', marginTop: '25%', backgroundColor: '#ffffff'}}>
                    <View style={{marginTop: '6%', flexDirection: 'row',}}>
                        <Text style={{color: '#000000', fontSize: 18, fontFamily: 'Poppins-Regular'}}>
                            Ai deja un cont?
                        </Text>
                        <Text style={{color: '#ffc22a', fontSize: 18, paddingLeft: 6, fontFamily: 'Poppins-SemiBold'}}>
                            Sign in
                        </Text>
                    </View>
                </TouchableOpacity>
            </TouchableOpacity>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

    container:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        height: '100%'
    },

    logo:{
        marginTop: '10%'
    },

    card:{
        width: '100%',
        backgroundColor: '#FFC22A',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
});
