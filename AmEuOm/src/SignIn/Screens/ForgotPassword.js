import React from 'react';
import {SafeAreaView} from "react-navigation";
import {
    Dimensions,
    Image,
    View,
    TouchableOpacity,
    Text,
    TextInput
} from 'react-native';
import Logo from "../Assets/lil_logo.svg";

export default function ForgotPassword(props)
{

    return (
        <SafeAreaView style={styles.container}>
            <Logo style={styles.logo}/>

            <View style={{flexDirection: 'column', marginTop: '30%', marginBottom: '40%'}}>
                <View style={{flexDirection: 'row', paddingVertical: 40}}>
                    <Text style={{fontSize: 27, color: '#000000', fontWeight: '600', paddingHorizontal: 5}}>
                        Am uitat
                    </Text>
                    <Text style={{fontSize: 27, color: '#ffffff', fontWeight: '600', paddingHorizontal: 5}}>
                        parola
                    </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{color: '#000000', fontSize: 18, paddingVertical: 20, paddingHorizontal: 3}}>
                        Introdu
                    </Text>
                    <Text style={{color: 'rgba(0,0,0,1)', fontSize: 18, fontWeight: '600', paddingVertical: 20, paddingHorizontal: 3}}>
                        emailul tau
                    </Text>
                </View>
                <View  style={[styles.textField, {width: '80%'}]}>
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16}}
                        placeholder='Email'
                        keyboardType={'email-address'}
                    />
                </View>
            </View>

            <TouchableOpacity style={styles.button}
                              // onPress={()=> {doResetPassword(email)}}
            >
                <Text style={{fontSize: 20, fontWeight: '600', color: '#000000'}}>Schimba parola</Text>
            </TouchableOpacity>

        </SafeAreaView>
    );
};

const styles = {

    container:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ff795f',
        paddingVertical: 60,
        height: '100%'
    },

    logo:{
        width: 128,
        height: 116.37,
        marginTop: 30,
        fill: '#000000'
    },

    textField: {
        width: 315,
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#000000',
        height: 40,
        borderRadius: 30,
    },

    button: {
        marginTop: 40,
        width: '80%',
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        height: 45,
        borderRadius: 30,
    },
};
