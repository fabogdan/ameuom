import React from 'react';
import {SafeAreaView} from "react-navigation";
import {
    Dimensions,
    Image,
    View,
    TouchableOpacity,
    Text,
    TextInput,
    StyleSheet,
    ScrollView
} from 'react-native';

import X from "../Assets/x.svg";
import Logo from "../Assets/lil_logo.svg";
import BackArrow from "../Assets/back_arrow.svg";
import Plus from "../Assets/plus.svg";

export default function CeStiiSaFaci(props)
{
    const {navigation} = props;

    const skill = (name, id) => (

        <View style={styles.skillCard}>
            <Text style={{fontSize: 14, fontFamily: 'Poppins-Regular'}}>
                {name}
            </Text>
            <TouchableOpacity style={{marginLeft: 5}} onPress={() => setSkills(skills.filter((x) => x !== name))}>
                <X/>
            </TouchableOpacity>
        </View>
    );

    const [skillName, setSkillName] = React.useState('');
    const [description, setDescription] = React.useState('');

    const [skills, setSkills] = React.useState([]);

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>

                <View style={{flexDirection: 'row', marginTop: 30, marginRight: '25%'}}>
                    <BackArrow/>
                    <Logo style={styles.logo}/>
                </View>

                <View style={{flexDirection: 'column'}}>

                    <View style={{flexDirection: 'row', marginTop: 45}}>
                        <Text style={{fontSize: 27, color: '#FDC32D', fontFamily: 'Poppins-SemiBold', paddingHorizontal: 5}}>
                            Ce stii
                            <Text style={{color: '#000000'}}> sa faci?</Text>
                        </Text>
                    </View>

                    <View style={{alignItems: 'center', justifyContent: 'center', alignContent: 'center', marginTop: 30}}>
                        <Text style={{fontSize: 14, fontFamily: 'Poppins-Regular'}}>
                            Scrie aici la ce te pricepi cel mai bine. Iti
                            recomandam sa oferi cat mai multe detalii si
                            sa tii seama de sugestiile care apar in timp ce
                            tastezi. In felul acesta ne vom asigura ca
                            primesti doar anunturile care te intereseaza.
                        </Text>
                    </View>

                    <View  style={styles.textField}>
                        <TextInput
                            placeholderTextColor={'#656565'}
                            style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                            value={skillName}
                            placeholder='Meserie'
                            onChangeText={setSkillName}
                        >
                        </TextInput>
                        <TouchableOpacity onPress={() => setSkills([...skills, skillName])} style={styles.plusCard}>
                            <Plus style={{fill: '#000000'}}/>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', flexWrap: 'wrap'}}>
                        { skills.map((name, id) => (skill(name, id)))}
                    </View>

                    <View style={{marginTop: '15%'}}>
                        <Text style={{fontSize: 24, fontFamily: 'Poppins-Regular'}}>
                            Descriere
                        </Text>
                        <Text style={{marginTop: '5%', fontSize: 14, fontFamily: 'Poppins-Regular'}}>
                            Spune-ne in detaliu cu ce te ocupi si la ce
                            esti priceput. Pentru fiecare indemanare
                            trecuta mai sus, detalieaza pe scurt
                            experienta ta. Aceasta descriere ne va ajuta
                            sa iti gasim clienti de calitate intr-un timp
                            scurt.
                        </Text>
                        <View style={styles.descriptionCard}>
                            <TextInput
                                placeholderTextColor={'#8c8c8c'}
                                multiline
                                style={{fontSize:16, marginTop: 15, marginLeft: 15, marginRight: 15, fontFamily: 'Poppins-Regular'}}
                                value={description}
                                placeholder='Descriere...'
                                onChangeText={setDescription}
                            />
                        </View>
                    </View>

                </View>

                <TouchableOpacity style={styles.button}
                    // onPress={()=> navigation.navigate('ClientSignUp')}
                >
                    <Text style={{fontSize: 20, fontFamily: 'Poppins-SemiBold', color: '#000000'}}>Inregistrare</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{flexDirection: 'row', height: 50, justifyContent: 'center', alignItems: 'center', marginTop: '5%', marginBottom: 5}}>
                    <Text style={{color: '#000000', fontSize: 18, fontFamily: 'Poppins-Regular'}}>
                        Ai deja un cont?
                    </Text>
                    <Text style={{color: '#ffc22a', fontSize: 18, paddingLeft: 6, fontFamily: 'Poppins-SemiBold'}}>
                        Sign in
                    </Text>
                </TouchableOpacity>

            </ScrollView>
        </SafeAreaView>
    );
};

const styles = {

    container:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        height: '100%',
        paddingHorizontal: '10%',
    },

    skillCard:{
        marginTop: 15,
        marginRight: 10,
        paddingHorizontal: 10,
        alignSelf: 'baseline',
        height: 30,
        borderRadius: 20,
        backgroundColor: '#FDC32D',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },

    plusCard:{
        width: 45,
        height: 40,
        borderBottomEndRadius: 30,
        borderTopEndRadius: 30,
        backgroundColor: '#FDC32D',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    },

    descriptionCard:{
        borderRadius: 10,
        borderColor: '#D8D8D8',
        borderWidth: 2,
        height: 190,
        marginTop: 20,
    },

    logo:{
        marginLeft: '30%',
        fill: '#000000'
    },

    textField: {
        paddingLeft: 30,
        marginTop: 20,
        marginBottom: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#FDC32D',
        height: 40,
        borderRadius: 30,
        fontFamily: 'Poppins-Regular'
    },

    button: {
        marginTop: '20%',
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffc22a',
        height: 45,
        borderRadius: 30,
    },
};
