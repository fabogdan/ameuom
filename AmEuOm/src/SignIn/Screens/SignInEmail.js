import React from 'react';
import {SafeAreaView} from "react-navigation";
import {
    Dimensions,
    Image,
    View,
    TouchableOpacity,
    Text,
    TextInput
} from 'react-native';
import SvgUri from 'react-native-svg';
import SVGImage from 'react-native-svg-image';
import Svg, {
    Circle,
    Ellipse,
    G,
    // Text,
    TSpan,
    TextPath,
    Path,
    Polygon,
    Polyline,
    Line,
    Rect,
    Use,
    // Image,
    Symbol,
    Defs,
    LinearGradient,
    RadialGradient,
    Stop,
    ClipPath,
    Pattern,
    Mask,
} from 'react-native-svg';
import { WebView } from 'react-native-webview';

import SignInPhone from "./SignInPhone";
import Logo from "../Assets/logo.svg";

export default function SignInEmail (props)
{
    const { navigation } = props;

    return (
        <SafeAreaView style={styles.container}>
            <Logo style={styles.logo}/>
            <View style={{paddingVertical: 50}}>
                <View style={[styles.textField, {marginTop: 60, width: '80%'}]}>
                    {/*<Image source={ require (require('../assets/ChangePassword/lock.png'))} style={styles.textFieldIcon} />*/}
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular',}}
                        // value={passwordValue}
                        placeholder='Email'
                        // icon={renderIcon}
                        // secureTextEntry={secureTextEntry}
                        // onIconPress={onIconPress}
                        // onChangeText={setPasswordValue}
                    />
                </View>
                <View style={[styles.textField, {marginTop: 20, width: '80%'}]}>
                    {/*<Image source={ require (require('../assets/ChangePassword/lock.png'))} style={styles.textFieldIcon} />*/}
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                        // value={passwordValue}
                        placeholder='Password'
                        // icon={renderIcon}
                        // secureTextEntry={secureTextEntry}
                        // onIconPress={onIconPress}
                        // onChangeText={setPasswordValue}
                    />
                </View>
            </View>

            {/*<View style={{flexDirection: 'column', alignItems:'center'}}>*/}
            <TouchableOpacity onPress={() => {}} style={styles.signIn}>
                <Text style={{fontSize: 20, color: '#000000', fontFamily: 'Poppins-SemiBold'}}>Sign in</Text>
            </TouchableOpacity>
            <Text style={{fontSize: 18, paddingVertical: 10, fontFamily: 'Poppins-Regular',}}>
                sau
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate('SignInPhone')} style={styles.signUp}>
                <Text style={{fontSize: 20, color: '#ffffff', fontFamily: 'Poppins-SemiBold'}}>Sign in </Text>
                <Text style={{fontSize: 20, color: '#ffffff', fontFamily: 'Poppins-Regular'}}>cu telefon</Text>
            </TouchableOpacity>
            {/*</View>*/}

            <View style={{flexDirection: 'column', alignItems: 'center', paddingVertical: 30}}>
                <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')}>
                    <Text style={{color: '#ffffff', fontSize: 18, fontFamily: 'Poppins-SemiBold'}}>
                        Am uitat parola
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('ChooseUserType')} style={{flexDirection: 'row', marginTop: 30}}>
                    <Text style={{color: '#000000', fontSize: 18, paddingHorizontal: 5, fontFamily: 'Poppins-SemiBold'}}>
                        Nu ai deja un cont?
                    </Text>
                    <Text style={{color: '#ffffff', fontSize: 18, paddingHorizontal: 5, fontFamily: 'Poppins-SemiBold'}}>
                        Sign Up
                    </Text>
                </TouchableOpacity>
            </View>

        </SafeAreaView>
    );
};

const styles = {

    container:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFC22A',
        paddingVertical: 180,
    },

    logo:{
        fill: '#000000',
    },

    textField: {
        width: 315,
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#000000',
        height: 40,
        borderRadius: 30,
        marginLeft: 30,
        marginRight: 30,
    },

    signIn: {
        width: '80%',
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        height: 40,
        borderRadius: 30,
        marginLeft: 30,
        marginRight: 30,
    },

    signUp: {
        width: '80%',
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000000',
        height: 40,
        borderRadius: 30,
        marginLeft: 30,
        marginRight: 30,
    },
};
