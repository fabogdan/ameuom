import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import SignInPhone from "../Screens/SignInPhone";
import SignInEmail from "../Screens/SignInEmail";
import ForgotPassword from "../Screens/ForgotPassword";
import ChooseUserType from "../../SignUp/ChooseUserType";
import CineEstiBeneficiar from "../../SignUp/SignUpBeneficiar/CineEstiBeneficiar";
import CineEstiPrestator from "../../SignUp/SignUpPrestator/CineEstiPrestator";
import UndePresteziServicii from "../../SignUp/SignUpPrestator/UndePresteziServicii";
import CeStiiSaFaci from "../../SignUp/SignUpPrestator/CeStiiSaFaci";

const Stack = createStackNavigator();

const SignInNavigation = () => {

    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{ gestureEnabled: true, headerShown: false,}}
            >
                <Stack.Screen
                    name='SignInPhone'
                    component={SignInPhone}
                />
                <Stack.Screen
                    name='ForgotPassword'
                    component={ForgotPassword}
                />
                <Stack.Screen
                    name='SignInEmail'
                    component={SignInEmail}
                />
                <Stack.Screen
                    name='ChooseUserType'
                    component={ChooseUserType}
                />
                <Stack.Screen
                    name='CineEstiBeneficiar'
                    component={CineEstiBeneficiar}
                />
                <Stack.Screen
                    name='CineEstiPrestator'
                    component={CineEstiPrestator}
                />
                <Stack.Screen
                    name='UndePresteziServicii'
                    component={UndePresteziServicii}
                />
                <Stack.Screen
                    name='CeStiiSaFaci'
                    component={CeStiiSaFaci}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
};

export default SignInNavigation
