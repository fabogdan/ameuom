import React from 'react';
import {SafeAreaView} from "react-native";
import {
    Dimensions,
    Image,
    View,
    TouchableOpacity,
    Text,
    TextInput,
    StyleSheet,
    ScrollView
} from 'react-native';
import Logo from "../Assets/lil_logo.svg";
import BackArrow from "../Assets/back_arrow.svg";

const secondaryAddress = (number) => (

    <View>
        <View style={{flexDirection: 'row', marginTop: 45}}>
            <Text style={{fontSize: 18, color: '#000000', fontFamily: 'Poppins-SemiBold'}}>
                {number}. Adresa secundara
            </Text>
        </View>

        <View  style={[styles.textField, {width: '100%'}]}>
            <TextInput
                placeholderTextColor={'#656565'}
                style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                // value={passwordValue}
                placeholder='* Localitate/Sector'
                // icon={renderIcon}
                // secureTextEntry={secureTextEntry}
                // onIconPress={onIconPress}
                // onChangeText={setPasswordValue}
            />
        </View>
        <View  style={[styles.textField, {width: '100%'}]}>
            <TextInput
                placeholderTextColor={'#656565'}
                style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                // value={passwordValue}
                placeholder='* Judet'
                // icon={renderIcon}
                // secureTextEntry={secureTextEntry}
                // onIconPress={onIconPress}
                // onChangeText={setPasswordValue}
            />
        </View>
        <View  style={[styles.textField, {width: '100%'}]}>
            <TextInput
                placeholderTextColor={'#656565'}
                style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                // value={passwordValue}
                placeholder='* Raza de acoperire (km)'
                // icon={renderIcon}
                // secureTextEntry={secureTextEntry}
                // onIconPress={onIconPress}
                // onChangeText={setPasswordValue}
            />
        </View>
    </View>
);

export default function CineEstiPrestator(props)
{
    const {navigation} = props;

    const [locationDependecy, setLocationDepencency] = React.useState(false);
    const [adreseSecundare, setAdreseSecundare] = React.useState(0);

    let addresses = [];

    for(let i = 0; i < adreseSecundare && i <= 3; i++)
    {
        addresses.push(secondaryAddress(i+2));
    }

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{flexDirection: 'row', marginTop: 30, marginRight: '25%'}}>
                    <BackArrow/>
                    <Logo style={styles.logo}/>
                </View>

                <View style={{flexDirection: 'column'}}>

                    <View style={{flexDirection: 'row', marginTop: 45}}>
                        <Text style={{fontSize: 27, color: '#FDC32D', fontFamily: 'Poppins-SemiBold', paddingHorizontal: 5}}>
                            Unde
                            <Text style={{color: '#000000'}}> prestezi servicii?</Text>
                        </Text>
                    </View>

                    <View style={{flexDirection: 'row', marginTop: 45}}>
                        <Text style={{fontSize: 18, color: '#000000', fontFamily: 'Poppins-SemiBold'}}>
                            1. Adresa principala
                        </Text>
                    </View>

                    <View  style={styles.textField}>
                        <TextInput
                            placeholderTextColor={'#656565'}
                            style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                            // value={passwordValue}
                            placeholder='* Localitate/Sector'
                            // icon={renderIcon}
                            // secureTextEntry={secureTextEntry}
                            // onIconPress={onIconPress}
                            // onChangeText={setPasswordValue}
                        />
                    </View>
                    <View  style={styles.textField}>
                        <TextInput
                            placeholderTextColor={'#656565'}
                            style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                            // value={passwordValue}
                            placeholder='* Judet'
                            // icon={renderIcon}
                            // secureTextEntry={secureTextEntry}
                            // onIconPress={onIconPress}
                            // onChangeText={setPasswordValue}
                        />
                    </View>
                    <View  style={styles.textField}>
                        <TextInput
                            placeholderTextColor={'#656565'}
                            style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                            // value={passwordValue}
                            placeholder='* Raza de acoperire (km)'
                            // icon={renderIcon}
                            // secureTextEntry={secureTextEntry}
                            // onIconPress={onIconPress}
                            // onChangeText={setPasswordValue}
                        />
                    </View>

                    { addresses }

                    <TouchableOpacity onPress={() => setAdreseSecundare(adreseSecundare + 1) } style={{flexDirection: 'row', marginTop: '5%'}}>
                        <Text style={{fontSize: 18, color: '#000000', fontFamily: 'Poppins-SemiBold'}}>
                            + <Text style={{color: '#FFC22A', fontFamily: 'Poppins-Regular'}}>Adauga adresa secundara</Text>
                        </Text>
                    </TouchableOpacity>

                    <View style={{flexDirection: 'row', marginTop: 45}}>
                        <Text style={{fontSize: 18, color: '#ffc22a', fontFamily: 'Poppins-Regular'}}>
                            * <Text style={{color: '#000000', fontFamily: 'Poppins-Regular'}}>Esti dependent de locatia ta?</Text>
                        </Text>
                    </View>

                    <View style={{flexDirection: 'row', marginTop: 15, marginLeft: 15}}>
                        <TouchableOpacity onPress={() => setLocationDepencency(true)} style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                            <View style={locationDependecy === true ? styles.fullCircle : styles.emptyCircle}>
                            </View>
                            <Text style={{fontSize: 20, marginLeft: 15, fontFamily: 'Poppins-Regular'}}>
                                Da
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => setLocationDepencency(false)} style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginLeft: '20%'}}>
                            <View style={locationDependecy === true ? styles.emptyCircle : styles.fullCircle}>
                            </View>
                            <Text style={{fontSize: 20, marginLeft: 15, fontFamily: 'Poppins-Regular'}}>
                                Nu
                            </Text>
                        </TouchableOpacity>
                    </View>

                </View>

                <TouchableOpacity style={styles.button}
                    onPress={()=> navigation.navigate('CeStiiSaFaci')}
                >
                    <Text style={{fontSize: 20, fontFamily: 'Poppins-SemiBold', color: '#000000'}}>Urmatorul</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{flexDirection: 'row', height: 50, justifyContent: 'center', alignItems: 'center', marginTop: '5%', marginBottom: 5}}>
                    <Text style={{color: '#000000', fontSize: 18, fontFamily: 'Poppins-Regular'}}>
                        Ai deja un cont?
                    </Text>
                    <Text style={{color: '#ffc22a', fontSize: 18, paddingLeft: 6, fontFamily: 'Poppins-SemiBold'}}>
                        Sign in
                    </Text>
                </TouchableOpacity>

            </ScrollView>
        </SafeAreaView>
    );
};

const styles = {

    container:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        height: '100%'
    },

    logo:{
        marginLeft: '30%',
        fill: '#000000'
    },

    textField: {
        paddingHorizontal: 30,
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#FDC32D',
        height: 40,
        borderRadius: 30,
        fontFamily: 'Poppins-Regular'
    },

    button: {
        marginTop: '20%',
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffc22a',
        height: 45,
        borderRadius: 30,
    },

    emptyCircle:{
        height: 12,
        width: 12,
        borderRadius: 20,
        backgroundColor: '#ffffff',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 2,
    },

    fullCircle:{
        height: 12,
        width: 12,
        borderRadius: 20,
        backgroundColor: '#ffc22a',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 2,
    }
};
