import React from 'react';
import {SafeAreaView} from "react-navigation";
import {
    Dimensions,
    Image,
    View,
    TouchableOpacity,
    Text,
    TextInput,
    StyleSheet
} from 'react-native';
import Tick from '../Assets/tick.svg'
import EmptyTick from '../Assets/empty_tick.svg'
import Logo from "../Assets/lil_logo.svg";
import BackArrow from "../Assets/back_arrow.svg";

export default function CineEstiBeneficiar(props)
{
    const [tick, setTick] = React.useState(false);
    const { navigation } = props;

    return (
        <SafeAreaView style={styles.container}>
            <View style={{flexDirection: 'row', marginTop: 30, marginRight: '25%'}}>
                <TouchableOpacity onPress={() => navigation.navigate('ChooseUserType')}>
                    <BackArrow/>
                </TouchableOpacity>
                <Logo style={styles.logo}/>
            </View>

            <View style={{flexDirection: 'column'}}>

                <View style={{flexDirection: 'row', marginTop: 45}}>
                    <Text style={{fontSize: 27, color: '#FDC32D', fontFamily: 'Poppins-SemiBold', paddingHorizontal: 5}}>
                        Cine
                        <Text style={{color: '#000000', fontFamily: 'Poppins-Regular'}}> esti?</Text>
                    </Text>
                </View>

                <View  style={[styles.textField, {width: '80%'}]}>
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                        // value={passwordValue}
                        placeholder='* Nume'
                        // icon={renderIcon}
                        // secureTextEntry={secureTextEntry}
                        // onIconPress={onIconPress}
                        // onChangeText={setPasswordValue}
                    />
                </View>
                <View  style={[styles.textField, {width: '80%'}]}>
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                        // value={passwordValue}
                        placeholder='* Prenume'
                        // icon={renderIcon}
                        // secureTextEntry={secureTextEntry}
                        // onIconPress={onIconPress}
                        // onChangeText={setPasswordValue}
                    />
                </View>
                <View  style={[styles.textField, {width: '80%'}]}>
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                        // value={passwordValue}
                        placeholder='* Email'
                        // icon={renderIcon}
                        // secureTextEntry={secureTextEntry}
                        // onIconPress={onIconPress}
                        // onChangeText={setPasswordValue}
                    />
                </View>
                <View  style={[styles.textField, {width: '80%'}]}>
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                        // value={passwordValue}
                        placeholder='* Numar telefon'
                        // icon={renderIcon}
                        // secureTextEntry={secureTextEntry}
                        // onIconPress={onIconPress}
                        // onChangeText={setPasswordValue}
                    />
                </View>
                <View  style={[styles.textField, {width: '80%'}]}>
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                        // value={passwordValue}
                        placeholder='* Parola'
                        // icon={renderIcon}
                        // secureTextEntry={secureTextEntry}
                        // onIconPress={onIconPress}
                        // onChangeText={setPasswordValue}
                    />
                </View>
                <View  style={[styles.textField, {width: '80%'}]}>
                    <TextInput
                        placeholderTextColor={'#656565'}
                        style={{flex:1, fontSize:16, fontFamily: 'Poppins-Regular'}}
                        // value={passwordValue}
                        placeholder='* Confirma parola'
                        // icon={renderIcon}
                        // secureTextEntry={secureTextEntry}
                        // onIconPress={onIconPress}
                        // onChangeText={setPasswordValue}
                    />
                </View>

                <View style={{marginTop: 30, justifyContent: 'space-evenly',alignContent:'center', flexDirection:'row', width: '70%'}}>
                    {
                        tick === false ?

                            <TouchableOpacity onPress={() => setTick(true)} style={{width: '15%', height: 30, alignItems: 'center', alignContent:'center'}}>
                                <EmptyTick style={{fill: '#000000'}}/>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={() => setTick(false)} style={{width: '15%', height: 30, alignItems: 'center', alignContent:'center'}}>
                                <Tick style={{fill: '#000000'}}/>
                            </TouchableOpacity>
                    }

                    <Text style={{fontSize: 18, marginLeft: 15, fontFamily: 'Poppins-Regular'}}>
                        Accept
                        <Text style={{fontFamily: 'Poppins-SemiBold', color: '#FFC22A'}}> termenii și condițiile </Text>
                        generale de utilizare a acestei
                        aplicații.
                    </Text>
                </View>
            </View>

            <TouchableOpacity style={styles.button}
                // onPress={()=> navigation.navigate('ClientSignUp')}
            >
                <Text style={{fontSize: 20, fontFamily: 'Poppins-SemiBold', color: '#000000'}}>Inregistrare</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate('SignInEmail')} style={{flexDirection: 'row', height: 50, width: '60%', justifyContent: 'center', alignItems: 'center', marginTop: '5%'}}>
                <Text style={{color: '#000000', fontSize: 18, fontFamily: 'Poppins-Regular'}}>
                    Ai deja un cont?
                </Text>
                <Text style={{color: '#ffc22a', fontSize: 18, paddingLeft: 6, fontFamily: 'Poppins-SemiBold'}}>
                    Sign in
                </Text>
            </TouchableOpacity>

        </SafeAreaView>
    );
};

const styles = {

    container:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        height: '100%'
    },

    logo:{
        marginLeft: '30%',
        fill: '#000000',
    },

    textField: {
        width: 315,
        paddingHorizontal: 30,
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#FDC32D',
        height: 40,
        borderRadius: 30,
    },

    button: {
        marginTop: 40,
        width: '80%',
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffc22a',
        height: 45,
        borderRadius: 30,
    },
};
