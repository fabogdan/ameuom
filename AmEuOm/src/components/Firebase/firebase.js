// import app from 'firebase/app';
// import 'firebase/auth';
// import 'firebase/firestore';
// import React from "react";
//
// const { uuid } = require('uuidv4');
//
// const config = {
//     apiKey: "AIzaSyDV87a9IX3BPI6IKedYjaUEcnJCuFV4SPA",
//     authDomain: "ameuom-4d633.firebaseapp.com",
//     databaseURL: "https://ameuom-4d633.firebaseio.com",
//     projectId: "ameuom-4d633",
//     storageBucket: "ameuom-4d633.appspot.com",
//     messagingSenderId: "247454290633",
//     appId: "1:247454290633:web:ee0c064ec113c1a7dab318",
//     measurementId: "G-059ZYCQLY7"
// };
//
// class Firebase {
//
//     constructor() {
//         app.initializeApp(config);
//
//         this.auth = app.auth();
//         this.db = app.firestore();
//     }
//
//     // *** Auth API ***
//
//     doCreateUserWithEmailAndPassword = (email, password, data) =>
//         this.auth.createUserWithEmailAndPassword(email, password)
//             .then(() => {
//                 this.db.collection('User')
//                     .doc(this.auth.currentUser.uid)
//                     .set(data)
//                     .then((res) => {
//                         sessionStorage.setItem('userLoggedIn', 'true')
//                         window.location.href = `/student/home?firstname=${data.firstname}`
//                     })
//             })
//
//     doSignInWithEmailAndPassword = (email, password) =>
//         this.auth.signInWithEmailAndPassword(email, password)
//             .then(() => { console.log(this.auth.currentUser.uid) })
//             .then(() => {
//                 this.db.collection('User')
//                     .doc(this.auth.currentUser.uid)
//                     .get()
//                     .then((res) => {
//                         sessionStorage.setItem('userLoggedIn', 'true')
//                         let userId = this.auth.currentUser.uid
//                         return { adminCheck: userId === 'V4sPgEbA0mM0IjwiZnhZQ2z2eMI2', res: res}
//                     })
//                     .then((result) => {
//                         if(result.adminCheck) {
//                             window.location.href = '/student/admin'
//                         } else {
//                             let expirationDate = result.res.data().licenseExpirationDate
//                             let today = new Date();
//                             let dd = String(today.getDate()).padStart(2, '0');
//                             let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
//                             let yyyy = today.getFullYear();
//
//                             today = mm + '/' + dd + '/' + yyyy;
//
//                             let expirationUnixTime = new Date(expirationDate).getTime() / 1000
//                             let todayUnixTime = new Date(today).getTime() / 1000
//
//                             if((expirationUnixTime - todayUnixTime) <= 0) alert("Please renew your license! Your license expired on " + expirationDate + "!")
//                             if(((expirationUnixTime - todayUnixTime) <= (7*24*60*60)) && ((expirationUnixTime - todayUnixTime) > 0))
//                             {
//                                 alert("You have less than a week left! Your license will expire on " + expirationDate + "!")
//                                 window.location.href = `/student/home?firstname=${result.res.data().firstname}`
//                             }
//                             if((expirationUnixTime - todayUnixTime) > (7*24*60*60))
//                                 window.location.href = `/student/home?firstname=${result.res.data().firstname}`
//                         }
//                     })
//             })
//
//     doSignOut = () => this.auth.signOut();
//
//     doPasswordReset = email => this.auth.sendPasswordResetEmail(email);
//
//     doPasswordUpdate = password =>
//         this.auth.currentUser.updatePassword(password);
//
//
//     // *** DB Interrogation API ***
//
//     // QUIZ Questions
//
//     doGetQuizQuestionsByCategory = (categoryName) =>
//         this.db.collection('Quiz Questions')
//             .where("category", "==", categoryName)
//             .get()
//             .then(function(querySnapshot) {
//                 let questions = [];
//                 querySnapshot.forEach(function(doc) {
//                     questions.push(doc.data());
//                 });
//
//                 return questions;
//             })
//
//     doGetQuizQuestionsByAllCategories = () =>
//         this.db.collection('Quiz Questions')
//             .get()
//             .then(function(querySnapshot) {
//                 let questions = [];
//                 querySnapshot.forEach(function(doc) {
//                     questions.push(doc.data());
//                 });
//
//                 return questions;
//             })
//
//     doPostQuizQuestionsOnCategory = (category, data) =>
//         this.db.collection('Quiz Questions')
//             .doc(uuid())
//             .set(data)
//
//
//     // EXAM Questions
//
//     doGetExamQuestionsByCategory = (categoryName) =>
//         this.db.collection('Exam Questions')
//             .where("category", "==", categoryName)
//             .get()
//             .then(function(querySnapshot) {
//                 let questions = [];
//                 querySnapshot.forEach(function(doc) {
//                     questions.push(doc.data());
//                 });
//
//                 return questions;
//             })
//
//     doGetExamQuestionsByAllCategories = (questionType, exam) =>
//         this.db.collection('Exam Questions')
//             .where("questionType", "==", questionType)
//             .where("exam", "==", exam)
//             .get()
//             .then(function(querySnapshot) {
//                 let questions = [];
//                 querySnapshot.forEach(function(doc) {
//                     questions.push(doc.data());
//                 });
//
//                 return questions;
//             })
//
//     doPostExamQuestionsOnCategory = (category, data) =>
//         this.db.collection('Exam Questions')
//             .doc(uuid())
//             .set(data)
//
//     doGetQuestionsFromMixCategories = (exam) =>
//         this.db.collection('Exam Questions')
//             .where("exam", "==", exam)
//             .get()
//             .then(function(querySnapshot) {
//                 let questions = [];
//                 querySnapshot.forEach(function(doc) {
//                     questions.push(doc.data());
//                 });
//
//                 return questions;
//             })
//
// }
//
// export default Firebase;
